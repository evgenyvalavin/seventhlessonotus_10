﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Text.Json;
using ChatterServer;
using System.Configuration;

namespace Chatter
{
    class Program
    {
        static string userName;
        private static string host = "vpn.ecsogy.ru"; //default
        private const int port = 9547;
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {
            if (ConfigurationManager.AppSettings["hostName"] != null)
                host = ConfigurationManager.AppSettings["hostName"];
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                client.Connect(host, port);
                stream = client.GetStream();

                var message = new Message
                {
                    TextMessage = userName,
                    SentDateTime = DateTime.Now
                };

                var data = JsonSerializer.SerializeToUtf8Bytes(message);
                stream.Write(data, 0, data.Length);

                Thread receiveThread = new Thread(ReceiveMessage);
                receiveThread.Start();
                Console.WriteLine("Greetings, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }

        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");
            while (true)
            {
                string message = Console.ReadLine();
                var message_obj = new Message
                {
                    TextMessage = message,
                    SentDateTime = DateTime.UtcNow
                };
                var data = JsonSerializer.SerializeToUtf8Bytes(message_obj);
                stream.Write(data, 0, data.Length);
            }
        }

        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64];
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);
                    InternalInfo internalInfo = JsonSerializer.Deserialize<InternalInfo>(builder.ToString());
                    if (internalInfo.MessageCode == 0)
                    {
                        data = new byte[64];
                        builder = new StringBuilder();
                        bytes = 0;
                        do
                        {
                            bytes = stream.Read(data, 0, data.Length);
                            builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                        }
                        while (stream.DataAvailable);
                        InternalErrors message_obj = JsonSerializer.Deserialize<InternalErrors>(builder.ToString());
                        Console.WriteLine(message_obj.ErrorMessage);
                    }
                    else
                    {
                        data = new byte[64];
                        builder = new StringBuilder();
                        bytes = 0;
                        do
                        {
                            bytes = stream.Read(data, 0, data.Length);
                            builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                        }
                        while (stream.DataAvailable);
                        Message message_obj = JsonSerializer.Deserialize<Message>(builder.ToString());
                        Console.WriteLine(message_obj.TextMessage + " - Time: " + message_obj.SentDateTime);
                    }
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!");
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
            Environment.Exit(0);
        }
    }
}

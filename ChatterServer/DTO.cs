﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatterServer
{
    [Serializable]
    public class InternalInfo
    {
        public byte MessageCode { get; set; }
    }

    [Serializable]
    public class Message
    {
        public string TextMessage { get; set; }
        public DateTime SentDateTime { get; set; }
    }

    [Serializable]
    public class InternalErrors
    {
        public byte ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}

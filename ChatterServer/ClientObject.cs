﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ChatterServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        string userName;
        TcpClient client;
        ServerObject server;

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                Message message_obj = GetMessage();
                userName = message_obj.TextMessage;

                string message = userName + " вошел в чат";
                server.BroadcastMessage(message, Id);
                Console.WriteLine(message_obj.TextMessage + ". Login time: " + message_obj.SentDateTime);
                while (true)
                {
                    try
                    {
                        message_obj = GetMessage();
                        if (message_obj != null)
                        {
                            message = message_obj.TextMessage;
                            message = String.Format("{0}: {1}", userName, message);
                            Console.WriteLine(message_obj.TextMessage + ". Sent time: " + message_obj.SentDateTime);
                            server.BroadcastMessage(message, Id);
                        }
                        else
                            server.NotifyErrorOccured(Id);
                    }
                    catch
                    {
                        message = string.Format("{0}: покинул чат", userName);
                        Console.WriteLine(message);
                        server.BroadcastMessage(message, Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                server.RemoveConnection(Id);
                Close();
            }
        }

        private Message GetMessage()
        {
            try
            {
                byte[] data = new byte[64];
                StringBuilder builder = new StringBuilder();
                do
                {
                    int bytes = Stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                }
                while (Stream.DataAvailable);
                var json_message = builder.ToString();
                return JsonSerializer.Deserialize<Message>(json_message);
            }
            catch(Exception e)
            {
                return null;
            }
        }

        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}
